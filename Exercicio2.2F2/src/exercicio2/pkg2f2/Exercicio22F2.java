/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio2.pkg2f2;
import java.util.Scanner;
/**
 *
 * @author Bruno
 */
public class Exercicio22F2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n=0, soma=0, i=0, numero=0;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.print("Introduza o nº de numeros a somar menor que 100:");
            n = scanner.nextInt();
        } while (n > 100);
        while (i < n) {
            System.out.println("Introduza um numero inteiro");
            numero = scanner.nextInt();
            if (numero % 3 == 0) {
                soma = soma + numero;
            }
            i++;
        }
        System.out.println("A soma dos numeros é:"+soma);
    }
    
}
